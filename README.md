

CS 426: Homework 2
Documentation and Puppet
Background

Installing an Apache web server on CentOS is a relatively straightforward process. Look up how to do it, either in the offical RH docs or in the first page of Google results. You should not need to edit the Apache configuration files.
Instructions

    By hand, configure the box to be a web server. Take notes! You will need to submit a document on how this is done by hand.
        Remember that you will need to enable port forwarding and open the firewall.
    Create a puppet module that configures the web server automatically.
    Create a vagrant project that applies your puppet module.
        Use my example DHCP vagrant project as a guide for how to load puppet modules with Vagrant.

Deliverables

    Create a Google Doc with instructions for installing the web server by hand (starting with Linux installed).
    Share the Google document with me and submit the share link on Moodle.
    Create a BitBucket repository called cs426-lab2
    Push your finished Vagrant project (including the puppet module) to Bitbucket. You do not need to make the puppet module a separate   
    git repo, but you are welcome to do so
    Grant read access to defreez_sou.

Requirements

You must use Apache (httpd) as your web server. Your instructions should be minimalist, but annotated. Any person comfortable with a command line should be able to follow them and have a working web server. I will clone your repo and run vagrant up. I should get a working web server.

Grade: 100%
