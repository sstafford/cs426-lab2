class apache
{
    package { 'httpd':
        ensure  => 'installed',
        before => Service['httpd'],
    }
    file { 'httpd.conf':
	path  => '/etc/httpd/conf/httpd.conf',
	owner => 'root',
	group => 'root',
        mode  => '0644',
    } 	

    service { 'httpd':
        ensure  => 'running',
        enable  => 'true',
	subscribe => File['/etc/httpd/conf/httpd.conf'],
    }

    file { 'iptables':
        path    => '/etc/sysconfig/iptables',
	owner   => 'root',
	group   => 'root',
	mode	=> '0600',
	source	=> 'puppet:///modules/apache/iptables',
    }
    
    exec { 'restore':
  	command =>  '/sbin/iptables-restore < /etc/sysconfig/iptables',
        subscribe => File['/etc/sysconfig/iptables'],
        refreshonly => 'true'
    }


}
